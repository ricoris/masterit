---
output: html_document
---

```{r}
library(tidyverse)
```

```{r}
pass <- .rs.askForPassword("")
my_db <- src_postgres(host = 'test-db01.math.aau.dk', 
                      dbname = 'demodb', 
                      user = 'demouser', 
                      password = pass)
src_tbls(my_db)

tbl(my_db, sql("SELECT * FROM flights"))

flights <- tbl(my_db, "flights")
flights

airports <- tbl(my_db, "airports")
airports

collect(airports)
```


```{r}
flights %>% 
  group_by(dest) %>% 
  summarise(n = n())

flights %>% 
  left_join(airports, by = c("dest" = "faa")) %>% 
  group_by(dest, name) %>% 
  summarise(n = n())

flights %>% 
  left_join(airports, by = c("dest" = "faa")) %>% 
  group_by(dest, name) %>% 
  summarise(n = n())
```

```{r}
d <- flights %>% 
  left_join(airports, by = c("dest" = "faa")) %>% 
  group_by(dest, name) %>% 
  summarise(n = n())
d
d %>% show_query()
d %>% explain()
```

```{r}
library(dbplyr)
dbplyr::translate_sql(if (x > 5) "big" else "small")
```


* MySQL
* MariaDB
* PostgreSQL
* Redshift
* SQLite
* ODBC
* BigQuery
* ...
* <https://dbplyr.tidyverse.org/articles/new-backend.html>
* <https://cran.r-project.org/web/views/Databases.html>



