---
title: | 
  | Miniproject in Introduction to Data Science
  | ITVEST Data Science and Big Data (DSBD)
output:
  pdf_document: 
    number_sections: true
    dev: png
  html_document: 
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, dpi = 300)
```

```{r, echo = TRUE}
library(tidyverse)
library(lubridate)
library(pander) # for prettier tables
library(scales) # for making prettier axes in plots
library(stringr)
library(forcats)
library(patchwork)

library(ggmap)
library(maps)
library(mapdata)

theme_set(theme_bw())

panderOptions('big.mark', ',')
```

# Formalia

Deadline for hand-in: Thursdag March 26 at 12:00 (noon).

Where: Moodle.

What: Both Rmd file and output (pdf or html).

Groups: Maximum 3 participants, however the project must be handed in individually.

Group members: Esben L. Kirkegaard, Steffen Dam, Rico Sørensen

# Exercises

Here, we focus on the `airlines`, `airports`, `flights`, `planes`, and `weather`  datasets:

```{r, echo = TRUE}
library(nycflights13)

airlines
airports
flights
planes
weather
```

Remember to read about the datasets.

# Exercises


## Exercise

Construct a barplot displaying number of flights per month (paying attention to axis labels and ticks).

```{r}
months <- c("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
mGFlights <- flights %>% 
            group_by(month) %>% 
            count(name="flights")

fPlot <- ggplot(mGFlights, aes(x=month, y=flights)) + 
         scale_x_continuous(breaks=seq(1,12, 1), labels=months) +
         geom_bar(stat="identity")
fPlot 

```

Now, in the barplot (showing number of flights per month), make a separate bar for each origin. Note that JFK must be the first level.

```{r}
origins <- c("JFK", "EWR", "LGA")
mOGFlights <- flights %>% 
  group_by(month, origin) %>% 
  count(name="flights")
ggplot(mOGFlights, aes(x=month, y=flights, fill=factor(origin, levels=origins))) + 
       scale_x_continuous(breaks=seq(1,12, 1), labels=months) + 
       geom_bar(stat="identity", position="dodge") +
       scale_fill_discrete("origin")
```

## Exercise

What are the top-10 destinations and how many flights were made to these?

```{r}
gDFlights <- flights %>% 
            group_by(dest) %>% 
            summarise(flights = n())

top10Dests <- gDFlights %>% 
  arrange(desc(flights)) %>% 
  data.frame() %>% 
  top_n(10)
top10Dests

```

For these 10 destinations, make a barplot illustrating the number of flights from origin to top-10 destination. Notice the legend and colours (`Paired` palette).

```{r}
flightsToPopDest <- flights %>% 
  filter(dest %in% top10Dests$dest) %>% 
  group_by(origin, dest) %>% 
  summarise("flights" =n())

makeFancy <- list(
  geom_bar(position="dodge"),
  scale_fill_brewer(palette="Paired"),
  guides(fill = guide_legend(title="Destination", nrow=5)),
  theme(legend.position = c(.15, .80), legend.background = element_rect("transparent"))
)

flightsToPopDest %>% 
  ggplot(aes(x=origin, y=flights, fill=dest)) +
  makeFancy
  

```

Now, order the bars (destinations) according to the total number of flights to these destinations.

```{r}
onlyDests <- top10Dests %>% 
  pull(dest)

flightsToPopDest %>% 
  ggplot(aes(x=origin, y=flights, fill=factor(dest, onlyDests))) +
  makeFancy

```




## Exercise

Is the mean air time from each of the three origins different? Further, are these differences statistically significant?

```{r}
meanAirByOrigin <- flights %>% 
  group_by(origin) %>% 
  summarise(meanAir = mean(air_time, na.rm=TRUE))
meanAirByOrigin
flights %>% 
  ggplot(aes(x = origin, y= air_time)) +
  geom_boxplot()
m <- lm(air_time~origin,data = flights)
confint(m)
```

Yes, the difference is significant.
As neither JFK nor LGA have confidence intervals spanning 0, we can determine with a 95% certainty, that the differences are statistically significant


## Exercise

How many weather observations are there for each origin?

```{r}
obsPerOri <- weather %>% 
  group_by(origin) %>% 
  count(name="count")
obsPerOri
  
```

Convert temperature to degrees Celsius. This will be used in the reminder of this miniproject.
(We do this for both `temp` and `dewp` using `mutate_at`)

```{r}
fahrToCels <- function(fahr) {
  return ((fahr - 32) * 5/9)
}
celsWeather <- weather %>% 
  mutate_at(c("temp", "dewp"), fahrToCels)
celsWeather
```

Construct a graph displaying the temperature at `JFK`.

```{r}
jfkTemps <- celsWeather %>% 
  filter(origin=="JFK")
jfkTempsPlot <- jfkTemps %>% 
  ggplot(aes(y = temp, x=time_hour)) +
  geom_line(stat="identity")
jfkTempsPlot
```

Add a red line showing the mean temperature for each day.

```{r}
getMeanTempPerDayForOrigin <- function(rawData, originName) {
  res <- rawData %>% 
    filter(origin == originName) %>% 
    group_by(year, month, day) %>% 
    summarize(temp = mean(temp)) %>% 
    mutate(time_hour=ISOdatetime(year, month, day, 12, 0, 0))
  return (res)
}
meanTempPerDayJFK <- celsWeather %>% 
  getMeanTempPerDayForOrigin("JFK")
  
jfkTempsPlot +
  geom_line(data=meanTempPerDayJFK, aes(y = temp, x = time_hour), color="red", size=1.4)

```

Now, visualuse the daily mean temperature for each origin airport.

```{r}
celsWeather %>% 
  group_by(origin, year, month, day) %>% 
  summarize(meanTemp = mean(temp)) %>% 
  mutate(time_day=ISOdatetime(year, month, day, 12, 0, 0)) %>% 
  ggplot(aes(x = time_day, y = meanTemp, color=origin)) +
  geom_line()

```



## Exercise

Investigate if arrival delay is associated with the flight distance (and also departure delay). 
Note how the plots are plotted in the same figure.

```{r}
install.packages(c("gridExtra", "hexbin"))
library(gridExtra)

stuff <- flights %>% 
  select(distance, arr_delay, dep_delay)
model <- lm(arr_delay~distance+dep_delay, stuff)
summary(model)
confint(model)

delays <- flights %>% 
  select(dep_delay, arr_delay, origin) %>% 
  drop_na()
distance <- flights %>% 
  select(distance, arr_delay, origin) %>% 
  drop_na()

cor1 <- cor(delays[c("dep_delay", "arr_delay")])
cor2 <- cor(distance[c("distance", "arr_delay")])
corPlotDel <- delays %>% 
  ggplot(aes(dep_delay, arr_delay)) +
  geom_point(alpha=0.9, size=2) +
  geom_hex(bins=100)
corPlotDis <- distance %>% 
  ggplot(aes(distance, arr_delay)) +
  geom_point(alpha=0.9, size=2) +
  geom_hex(bins=100)
grid.arrange(corPlotDel, corPlotDis, ncol=2)
```



## Exercise

Investigate if departure delay is associated with weather conditions at the origin airport. This includes descriptives (mean departure delay), plotting, regression modelling, considering missing values etc.

```{r}
install.packages(c("corrplot", "caTools"))

library("corrplot")
library("caTools")
noNAFlights <- flights%>%
  select(year, month, day, origin, hour, arr_delay, dep_delay, tailnum) %>% 
  drop_na()

noNAWeather <- weather %>% 
  drop_na()

flightWeather <- merge(x=noNAFlights, y=noNAWeather, by=c("origin", "year", "month", "day", "hour")) %>% 
  select(dep_delay, humid, precip, visib, wind_dir, wind_speed, temp, dewp)

corDelayWeather <- cor(flightWeather)
corDelayWeather
corDelayWeather %>% 
corrplot(method="color", type = "upper",
         tl.srt = 45,addCoef.col = "black")

set.seed(1324) #Setting seed to avoid varying data depending on when it's run
sample <- sample.split(flightWeather$dep_delay, SplitRatio = 0.7)
trainSet <- subset(flightWeather, sample == T)
testSet <- subset(flightWeather, sample == F)

weatherModel <- lm(dep_delay~., data = trainSet)
summary(weatherModel)
resWeather <- residuals(weatherModel)
resWeather<- as.data.frame(resWeather)

resWeather %>% 
  ggplot(aes(resWeather)) +
  geom_histogram(fill="red", alpha=0.5, bins=20)

delPrediction <- predict(weatherModel, testSet)
delPreRes <- cbind(delPrediction, testSet$dep_delay)

colnames(delPreRes) <- c("predict", "actual")
head(delPreRes)
```


## Exercise

Is the age of the plane associated to delay?

```{r}
noNAFlights <- flights %>% 
  select(tailnum, arr_delay, dep_delay) %>% 
  drop_na()
noNAPlanes <- planes %>% 
  select(tailnum, year) %>% 
  drop_na()

mergedData <- merge(x=noNAFlights,y=noNAPlanes, by="tailnum")
cur_year <- as.integer(format(Sys.Date(), "%Y"))
dataWithAge <- mergedData %>% 
  mutate(age=cur_year-year) %>% 
  group_by(age) %>% 
  summarise(meanDelay=mean(arr_delay), count=n()) %>%
  filter(count > 200) %>% 
  arrange(desc(age))
ageModel <- lm(meanDelay~age, dataWithAge)

dataWithAge %>% 
  ggplot(aes(x=age, y=meanDelay)) +
  geom_line()
summary(ageModel)
confint(ageModel)
```

## Exercise

It seems like the plane manufacturer could use a cleaning. After that, how many manufacturers have more than 200 planes? And how many flights are each manufacturer with more than 200 planes responsible for?

```{r}
cleanPlanes <- planes %>% 
  filter(!is.na(manufacturer))

bigManus <- cleanPlanes %>% 
  group_by(manufacturer) %>% 
  summarise(count = n()) %>% 
  arrange(desc(count)) %>% 
  filter(count > 200)

merge(cleanPlanes, flights, by="tailnum") %>% 
  filter(manufacturer %in% bigManus$manufacturer) %>%
  group_by(manufacturer) %>% 
  summarise(count = n()) %>%  
  arrange(desc(count))
```


## Exercise

It turns out that Airbus has several main models, e.g. several A320 (A320-211, A320-212 etc.) and so on. Create a frequency table of the main models for Airbus and how many planes there are in each.

```{r}
airbusModels <- planes %>% 
  filter(manufacturer == "AIRBUS") %>%
  mutate(mainModel = substr(model, 1, 4))
tbl <- table(airbusModels$mainModel)
tbl

```





## Exercise

Are larger planes (measured by number of seats) more or less delayed than smaller planes?

```{r}
mergedData <- merge(x=flights,y=planes, by="tailnum")
sizeModel <-  lm(arr_delay~seats,mergedData)
summary(sizeModel)
confint(sizeModel)

selectiveMergedData <- mergedData %>% 
  filter(!is.na(seats) & !is.na(arr_delay) & !is.na(dep_delay)) %>% 
  select(arr_delay, seats, dep_delay)
corModel <- cor(selectiveMergedData)corrplot(corModel)
corModel %>% 
  corrplot(method="color", type = "upper",
         tl.srt = 45,addCoef.col = "black")

```


## Exercise

On a map (`map_data("usa")`), plot the airports that has flights to them.

```{r}
usa <- map_data("usa")
mergedAirportData <- merge(x=airports,y=flights, by.x="faa", by.y="dest")

groupedByFaa <- mergedAirportData %>% 
  group_by(lon, lat) %>% 
  summarise(total=n())
```

```{r}
ggplot() + 
  geom_polygon(data = usa, aes(x = long, y = lat, group = group), fill = "grey") + 
  coord_quickmap() +
  geom_point(data=groupedByFaa, aes(x = lon, y = lat, color="red"))
#Adding size=total to geom_point aesthetics will increase the size of the points based on how many flights are to the destination
```

```{r}

```

