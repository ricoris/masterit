---
title: "Factors with forcats"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, comment = NA)
```

```{r}
library(tidyverse)
theme_set(theme_bw())
library(forcats)
```

# Factors

* Factors/faktorer er **kategoriske variabler**
* **Besværlige** at håndtere i base R i grafik, tabeller mm.
* Pakke: `forcats`

```{r}
x1 <- c("Dec", "Apr", "Jan", "Mar")
x2 <- c("Dec", "Apr", "Jam", "Mar")
month_levels <- c(
  "Jan", "Feb", "Mar", "Apr", "May", "Jun", 
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
)
```

```{r}
sort(x1)
sort(factor(x1, levels = month_levels))
```

Andre værdier end i `levels` giver NA:

```{r}
factor(x2, levels = month_levels)
```

# forcats

* `fct_` præfiks

Brug rækkefølge værdier optræder i i stedet for alfabetisk.

`fct_inorder()`:

```{r}
x1 %>% factor() %>% fct_inorder()
```

# Eksempel

`gss_cat` data:

```{r}
head(gss_cat)
str(gss_cat)
```

```{r}
gss_cat %>%
  count(race)
```

`fct_rev()`:

```{r}
levels(gss_cat$race)
levels(fct_rev(gss_cat$race))
```

```{r}
gss_cat %>%
  mutate(race = fct_rev(race)) %>% 
  count(race)
```

```{r}
gss_cat %>%
  mutate(race = fct_rev(race)) %>% 
  count(race, marital)
```

```{r}
gss_cat %>%
  mutate(race = fct_rev(race)) %>% 
  count(race, marital) %>% 
  pivot_wider(names_from = marital, values_from = n)
```

## Modifying the ordering 

```{r}
relig_summary <- gss_cat %>%
  group_by(relig) %>%
  summarise(
    age = mean(age, na.rm = TRUE),
    tvhours = mean(tvhours, na.rm = TRUE),
    n = n()
  )
relig_summary
ggplot(relig_summary, aes(tvhours, relig)) + geom_point()
```

`fct_reorder()`:

```{r}
ggplot(relig_summary, aes(tvhours, fct_reorder(relig, tvhours))) +
  geom_point()
```

`fct_relevel()`:

```{r}
days <- c("Mon", "Tue", "Weds", "Thur", "Fri", "Sat", "Sun") %>%
  factor() %>% fct_inorder()
days
fct_relevel(days, "Sun", after = 0) ## Week starts with Sunday
```

## Ændring af levels

* `fct_recode()`
    + `"new level" = "old level"`
* `fct_collapse()`
* `fct_lump()`

```{r}
gss_cat %>%
  mutate(partyid = fct_recode(partyid,
    "Republican, strong"    = "Strong republican",
    "Republican, weak"      = "Not str republican",
    "Independent, near rep" = "Ind,near rep",
    "Independent, near dem" = "Ind,near dem",
    "Democrat, weak"        = "Not str democrat",
    "Democrat, strong"      = "Strong democrat"
  )) %>%
  count(partyid)
```

`fct_collapse()`:

```{r}
gss_cat %>%
  mutate(partyid = fct_collapse(partyid,
    other = c("No answer", "Don't know", "Other party"),
    rep = c("Strong republican", "Not str republican"),
    ind = c("Ind,near rep", "Independent", "Ind,near dem"),
    dem = c("Not str democrat", "Strong democrat")
  )) %>%
  count(partyid)
```

`fct_lump()`:

```{r}
gss_cat %>%
  mutate(relig = fct_lump(relig, n = 10, other_level = "Other")) %>%
  count(relig, sort = TRUE) %>%
  print(n = Inf)
```
